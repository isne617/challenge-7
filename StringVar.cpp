#include <iostream>
#include "strvar.h"
using namespace std;
using namespace strvarken;

void conversation(int max_name_size);

int main() 
{
	using namespace std;
	conversation(30);
	cout << "\nEnd of Demo.\n";

	system("cmd /c pause");
	return 0;
}


//Demo Func
void conversation(int max_name_size)
{

	StringVar s1(max_name_size),s2(max_name_size);
	StringVar mixed;
	int point,start,stop;
	char setletter;

	cout << "Input someword  \n>> Sentance 1: ";   //input sentance 1
	cin >> s1;
	cout << "Where position do you want to change [sentance 1]\n>> Position 1: ";  //position do you want to change
	cin >> point;
	cout << ">> " << s1.one_char(point);                                          //show character
	cout << "\nWhat character do you want to add [sentance 1]\n>> Character 1: ";   //character to add
	cin >> setletter;
	s1.set_char(point,setletter);                                                  //set letter
	cout << ">> New sentance 1: " << s1 << endl;                                     //show new sentance1

	cout << "\nInput word do you want to add \n>> Sentance 2: "; //input sentance 1
	cin >> s2;
	cout << "Where position do you want to change [sentance 2]\n>> Position 2: ";  //position do you want to change
	cin >> point;
	cout << ">> " <<s2.one_char(point);                                           //show character
	cout << "\nWhat character do you want to add [sentance 2]\n>> Character 2: ";  //character to add
	cin >> setletter;
	s2.set_char(point,setletter);                                                //set letter
	cout << ">> New sentance 2: "<< s2 << endl << endl;                            //show new sentance2

	cout << ">> " << s1;
	if(s1 == s2)                          //if sentance 1 equal to sentance 2 show =
	{
		cout << " = ";
	}
	else                                 //if sentance 1 not equal to sentance 2 show !=
	{
		cout << " != ";
	}
	cout << s2<<endl;

	mixed = s1+s2;
	cout << ">> Mixed sentance: "<<mixed << endl;     //show s1+s2
	cout << "Where position do you want to start\n>> Start: ";  //position start to copy
	cin >> start;
	cout << "Where position do you want to stop\n>> Stop: ";   //position stop to copy
	cin >> stop;
	cout << ">> Sentance : " << mixed.copy_piece(start,stop)<<endl; //show sentance to finish





}

#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarken
{
	//Uses cstddef and cstdlib
	StringVar::StringVar(int size) : max_length(size)  //constructor when size of string is specified
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar() : max_length(100) //constructor with default size of 100
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const char a[]) : max_length(strlen(a))   //constructor using array as values
	{
		value = new char[max_length + 1];

		for(int i = 0;i < strlen(a);i++)
		{
			value[i] = a[i];
		}
		value[strlen(a)] = '\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length()) //copy constructor
	{
		value = new char[max_length + 1];
		for(int i = 0;i < strlen(string_object.value);i++)
		{
			value[i] = string_object.value[i];
		}
		value[strlen(string_object.value)] = '\0';
	}

	StringVar::~StringVar()
	{
		delete [] value;
	}


	char StringVar::one_char(int point) //function return a single charactor.
	{
		return value[point-1];
	}

	void StringVar::set_char(int point,char setletter) //function change a single charactor.
	{
		value[point-1] =setletter;
	}

 	StringVar operator +(StringVar s1 , StringVar s2)  //Function overload version of the + operator to allow stringvar concatenation
	{
		StringVar newsentance;  //stringvar for input after allow stringvar concatenation
		for(int i = 0 ; i < s1.length() ; i++)
		{
			newsentance.value[i] = s1.value[i];       
		}
		for(int i = s1.length() ; i <= (s2.length() + s1.length()) ;i++)
		{
			if (i == s2.length() + s1.length())
			{
				newsentance.value[i]='\0';  //if fully	newsentance already,break it.
				break;
			}
			newsentance.value[i]=s2.value[i-s1.length()];
		}
	return	newsentance;

	}

	bool operator == (StringVar s1 ,StringVar s2) //Function overload version of the == operator
	{	
		if(s1.length() == s2.length())      
		{
			for(int i =0 ; i <= s1.length() ; i++)  
			{
				if(s1.value[i] != s2.value[i]) //check one after another
				{
					return false;
				}
					
			}return true;
		}
		else
		{
			return false;
		}
	}

	StringVar StringVar::copy_piece(int start,int stop)  //Function return a specified substring
	{
		StringVar copy;                
		for(int i=0;i<=stop-start;i++)
		{
			copy.value[i]=value[start+i-1];
		}
		copy.value[stop-start+1]='\0';
		return copy;

	}

	//Uses cstring
	int StringVar::length() const //return the length of the current string
	{
		return strlen(value);
	}

	//Uses iostream
	void StringVar::input_line(istream& ins) 
	{
		ins.getline(value, max_length + 1);
	}

	//Uses iostream
	ostream& operator << (ostream& outs, const StringVar& the_string) //Function overload version of extraction operator << .
	{
		outs << the_string.value;
		return outs;
	}

	istream& operator >> (istream& ins, const StringVar& the_string) //Function overload version of extraction operator >> .
	{
		ins >> the_string.value;
		return ins;
	}

}


#ifndef STRVAR_H
#define STRVAR_H

#include <iostream>

using namespace std;

namespace strvarken
{
	class StringVar
	{
	public:
		StringVar(int size);  //constructor when size of string is specified
		StringVar();  //constructor with default size of 100
		StringVar(const char a[]);  //constructor using array as values

		StringVar(const StringVar& string_object);   //copy constructor
		~StringVar();  

		char one_char(int point);  //function return a single charactor.
		void set_char(int point,char setletter); //function change a single charactor.

		friend StringVar operator +(StringVar s1,StringVar s2);  //Function overload version of the + operator to allow stringvar concatenation
		friend bool operator == (StringVar s1,StringVar s2); //Function overload version of the == operator
		StringVar copy_piece(int start ,int stop );  //Function return a specified substring

		int length() const;  //return the length of the current string
		void input_line(istream& ins);

		friend ostream& operator<<(ostream& outs, const StringVar& the_string);  //Function overload version of extraction operator << .
		friend istream& operator>>(istream& ins, const StringVar& the_string);  //Function overload version of extraction operator >> .

	private:
		char *value;
		int max_length;
	};
}

#endif //STRVAR_H